--This is a snippet for combining duplicate part's quantity.

    declare 
      l_sub_form_id   number;
      l_sub_form_name  varchar2(64);
      sub_from_ar     json_list;
      l_empty_checker    number:=0; --check 4 repeating form
      --type tbl_var is table of varchar2(64);
      --ar_part_number tbl_var:=tbl_var();
      --ar_part_number_dist tbl_var:=tbl_var();
      o_part_number varchar2(64);
      ar_part_no_orig at_var_array:=at_var_array();
      ar_part_no_dist at_var_array:=at_var_array();
      ar_dup_idx at_var_array;
      ar_qty_request at_var_array:=at_var_array();
      ar_remove_idx at_var_array:=at_var_array();
      l_remove_idx_cnt number := 0;
      l_part_col_label varchar2(8);
      l_qty_col_label varchar2(8);
      l_sum_qty number;
      l_temp_json json;

      function get_arr_idx(p_tbl in at_var_array, p_col in varchar2) return at_var_array is
          dup_idx at_var_array;
      begin
          select rn 
          bulk collect into dup_idx from (
            select column_value cv, rownum rn
             from table(p_tbl) 
          )
          where cv = p_col;
          return dup_idx;
      end get_arr_idx;

    begin 
      for i in 1..l_collection_count loop
        l_fname := 'p_' || lpad(i,2,'0');
          if t_form_el_config(i).sub_form_id is not null then
            l_sub_form_id :=t_form_el_config(i).sub_form_id;
            l_sub_form_name :=sub_form_name(l_sub_form_id);
            sub_from_ar :=fweb_util.get_sub_form_ar(obj,l_fname);
            
            if l_sub_form_name='ISS_REQ_ITEM' and sub_from_ar.count=0 then
              l_empty_checker := l_empty_checker+1;
                if l_empty_checker = 2 then
                    g_error := 'Please add parts to request.';
                    raise g_ex;
            end if;
          end if;

          if sub_from_ar.count=0 then
            continue;
          end if;

              begin
                select 'p_' || lpad(disp_col_order,2,'0') into l_part_col_label 
                from fweb_form_element_config 
                where form_id=l_sub_form_id 
                and col_name='PART_NUMBER';

                select 'p_' || lpad(disp_col_order,2,'0') into l_qty_col_label
                from fweb_form_element_config 
                where form_id=l_sub_form_id 
                and col_name='QTY_TO_REQUEST';
              exception
                when others then
                  g_error:='Error while finding subform key.';
                  raise g_ex;
              end;

              FOR m IN 1 .. sub_from_ar.count LOOP
                ar_part_no_orig.extend;
                ar_qty_request.extend;
                ar_part_no_orig(m) := json_ext.get_string(json(sub_from_ar.get(m)),l_part_col_label);
                ar_qty_request(m) := json_ext.get_string(json(sub_from_ar.get(m)),l_qty_col_label);
              END LOOP;

              --ar_part_no_orig := fweb_util.EXTRACT_SINGLE_COL_EXCL_DEL_V2(obj,l_fname,l_sub_form_id,'PART_NUMBER');
              ar_part_no_dist := SET(ar_part_no_orig);
              if (ar_part_no_orig.count != ar_part_no_dist.count) then
                ar_part_no_dist := SET(ar_part_no_orig MULTISET except ar_part_no_dist);
 
    
                FOR k IN 1 .. ar_part_no_dist.count LOOP
                  ar_dup_idx := at_var_array();
                  ar_dup_idx := get_arr_idx(ar_part_no_orig,ar_part_no_dist(k));
                  l_sum_qty := 0;

    
                  FOR l IN 1 .. ar_dup_idx.count LOOP
                    l_sum_qty := l_sum_qty + nvl(ar_qty_request(ar_dup_idx(l)),0);
                    if l = ar_dup_idx.count then
                      l_temp_json := json();
                      l_temp_json := json(sub_from_ar.get(ar_dup_idx(l)));
                      l_temp_json.remove(l_qty_col_label);
                      l_temp_json.put(l_qty_col_label,to_char(l_sum_qty));
                      sub_from_ar.replace(to_number(ar_dup_idx(l)),l_temp_json.to_json_value);
                      continue;
                    end if;
                    l_remove_idx_cnt := l_remove_idx_cnt+1;
                    ar_remove_idx.extend;
                    ar_remove_idx(l_remove_idx_cnt) := ar_dup_idx(l);
                  END LOOP;
                END LOOP;

                FOR n IN REVERSE 1 .. ar_remove_idx.count LOOP
                  sub_from_ar.remove(ar_remove_idx(n));
                END LOOP;
              end if;

          FOR j in 1..sub_from_ar.count loop
            if l_sub_form_name='ISS_REQ_ITEM' then
              --null;
              --fweb_util.log_trace('proc_sub_json',sub_from_ar.get(j).to_char);commit;
              PROC_IR_PART_ENTRY(l_sub_form_id,l_md_number,l_warehouse,json(sub_from_ar.get(j)),o_part_number);
              --ar_part_number.extend;
              --ar_part_number(j):=o_part_number;
            end if;
          END LOOP;
            /*ar_part_number_dist := ar_part_number;
            ar_part_number_dist := ar_part_number_dist MULTISET UNION DISTINCT ar_part_number;
            if ar_part_number_dist.count!=ar_part_number.count then
              g_error:='You''ve added the same part twice to this request. Remove one of them and re-submit your request';
              raise g_ex;
            end if;*/
        end if;
      end loop;
    end;